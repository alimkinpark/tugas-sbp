import { Outlet } from "react-router-dom";

export default function Root() {
    return (
        <>

            <nav className="navbar navbar-expand-lg bg-body-tertiary bg-dark navbar-dark" data-bs-theme="dark">
                <div className="container">
                    <a className="navbar-brand" href="#">Diagnosis Penyakit</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <a className="nav-link" aria-current="page" href="#">Home</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="konsultasi">Konsultasi</a>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Master Data
                                </a>
                                <ul className="dropdown-menu">
                                    <li><a className="dropdown-item" href="penyakit">Penyakit</a></li>
                                    <li><a className="dropdown-item" href="gejala">Gejala</a></li>
                                    <li><a className="dropdown-item" href="aturan">Aturan (rule)</a></li>
                                    <li><a className="dropdown-item" href="restore">Restore Data</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div className="container mt-4">
                <Outlet />
            </div>
        </>
    );
}