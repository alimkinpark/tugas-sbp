import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    aturan : JSON.parse(localStorage.getItem('aturan')) ?? []
}

export const aturanSlice = createSlice({
    name : 'aturan',
    initialState,
    reducers: {
        addAturan: (state, action) => {
            let obj = state.aturan.filter(py=> py.idPenyakit === action.payload.idPenyakit)

            if (obj.length > 0) {
                state.aturan = [...state.aturan.filter(py=>py.idPenyakit !== action.payload.idPenyakit), action.payload]
            } else {
                state.aturan = [...state.aturan, action.payload]
            }

            localStorage.setItem('aturan', JSON.stringify(state.aturan));
        },
        deleteAturan: (state, action) => {
            state.aturan = [...state.aturan.filter(py=>py.idPenyakit !== action.payload)]
            localStorage.setItem('aturan', JSON.stringify(state.aturan));
        }
    }
})

export const {addAturan, deleteAturan} = aturanSlice.actions

export const getAturan = (state)=> state.aturan.aturan;

export default aturanSlice.reducer