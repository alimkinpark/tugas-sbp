import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    penyakit : JSON.parse(localStorage.getItem('penyakit')) ?? []
}

export const penyakitSlice = createSlice({
    name : 'penyakit',
    initialState,
    reducers: {
        addPenyakit: (state, action) => {
            let obj = state.penyakit.filter(py=> py.idPenyakit === action.payload.idPenyakit)

            if (obj.length > 0) {
                state.penyakit = [...state.penyakit.filter(py=>py.idPenyakit !== action.payload.idPenyakit), action.payload]
            } else {
                state.penyakit = [...state.penyakit, action.payload]
            }

            localStorage.setItem('penyakit', JSON.stringify(state.penyakit));
        },
        deletePenyakit: (state, action) => {
            state.penyakit = [...state.penyakit.filter(py=>py.idPenyakit !== action.payload)]
            localStorage.setItem('penyakit', JSON.stringify(state.penyakit));
        }
    }
})

export const {addPenyakit, deletePenyakit} = penyakitSlice.actions

export const getPenyakit = (state)=> state.penyakit.penyakit;

export default penyakitSlice.reducer