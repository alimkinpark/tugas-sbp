import {createSlice} from '@reduxjs/toolkit';

const initialState = {
    gejala: JSON.parse(localStorage.getItem('gejala')) ?? []
}

export const gejalaSlice = createSlice({
    name: 'gejala',
    initialState,
    reducers: {
        addGejala: (state, action) => {
            let obj = state.gejala.filter(py=> py.idGejala === action.payload.idGejala)

            if (obj.length > 0) {
                state.gejala = [...state.gejala.filter(py=>py.idGejala !== action.payload.idGejala), action.payload]
            } else {
                state.gejala = [...state.gejala, action.payload]
            }

            localStorage.setItem('gejala', JSON.stringify(state.gejala));
        },
        deleteGejala: (state, action) => {
            state.gejala = [...state.gejala.filter(py=>py.idGejala !== action.payload)]
            localStorage.setItem('gejala', JSON.stringify(state.gejala));
        }
    }
})

export const {addGejala, deleteGejala} = gejalaSlice.actions

export const getGejala = (state)=>state.gejala.gejala

export default gejalaSlice.reducer