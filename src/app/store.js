import {configureStore} from '@reduxjs/toolkit';
import penyakitReducer from '../features/penyakit/penyakitSlice';
import gejalaReducer from '../features/gejala/gejalaSlice';
import aturanReducer from '../features/aturan/aturanSlice';

export default configureStore({
    reducer:{
        penyakit: penyakitReducer,
        gejala: gejalaReducer,
        aturan: aturanReducer
    }
})