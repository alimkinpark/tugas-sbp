import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Penyakit from "./Pages/Penyakit";
import Gejala from "./Pages/Gejala";
import Aturan from "./Pages/Aturan";
import Restore from "./Pages/Restore";
import Konsultasi from "./Pages/Konsultasi";

import ErrorPage from "./error-page";

import Root from "./routes/root";

import "bootstrap/dist/css/bootstrap.min.css";
import "jquery/dist/jquery.min.js";
import "bootstrap/dist/js/bootstrap.min.js";

import store from "./app/store";
import { Provider } from "react-redux";

const router = createBrowserRouter([
  {
    path: "/",
    // element: <div>Hello world!</div>,
    element: <Root />,
    errorElement: <ErrorPage />,
    // children: [
    //   {
    //     path: "/#penyakit",
    //     element: <testing />,
    //   },
    // ],
    children: [
      {
        path: "penyakit",
        element: <Penyakit />,
      },
      {
        path: "gejala",
        element: <Gejala />,
      },
      {
        path: "aturan",
        element: <Aturan />,
      },
      {
        path: "restore",
        element: <Restore />,
      },
      {
        path: "konsultasi",
        element: <Konsultasi />,
      },
    ],
  },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
