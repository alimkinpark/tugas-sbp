import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getGejala } from "../features/gejala/gejalaSlice";
import { getPenyakit } from "../features/penyakit/penyakitSlice";
import { addAturan, deleteAturan, getAturan } from "../features/aturan/aturanSlice";

import Select from "react-select";
import makeAnimated from 'react-select/animated';

export default function Aturan() {
  const dataGejala = useSelector(getGejala);
  const dataPenyakit = useSelector(getPenyakit);
  const dataAturan = useSelector(getAturan);
  const dispatch = useDispatch();

  let btnCancel;
  const [aturan, setAturan] = useState({
    idPenyakit: '',
    idGejala: [],
  });

  const [isDisabled, setIsDisabled] = useState(false);

  const handleChangePenyakit = (event)=>{
    // console.log(event)
    setAturan({...aturan, idPenyakit: event.value})
  }

  const handleChangeGejala = (event) => {
    // console.log(event)
    // setGejala(Array.isArray(event) ? event.map(x=>x.value):[])
    setAturan({...aturan, idGejala: Array.isArray(event) ? event.map(x=>x.value):[]})

    if(event.length) {
        setIsDisabled(true)
    } else {
        setIsDisabled(false)
    }
  };

  const handleSimpan = (event) => {
    event.preventDefault();

    dispatch(addAturan(aturan));
    handleCancel();
  };

  const handleEdit = (id) => {
    btnCancel = document.querySelector("#cancel");
    btnCancel.style.display = "inline";

    const data = dataAturan
      .filter((ms) => {
        return ms.idPenyakit === id;
      })
      .map((newPy) => {
        return newPy;
      });

    // console.log(data);
    setIsDisabled(true);
    setAturan(data[0]);
  };

  const handleCancel = () => {
    btnCancel = document.querySelector("#cancel");
    btnCancel.style.display = "none";
    setIsDisabled(false);
    setAturan({ idPenyakit: "", idGejala: [] });
  };

  const handleDelete = (id) => {
    dispatch(deleteAturan(id));
  };

  const options = dataGejala.map((gejala) => {
    return { value: gejala.idGejala, label: gejala.namaGejala };
  });

  const options2 = dataPenyakit.map((penyakit) => {
    return { value: penyakit.idPenyakit, label: penyakit.namaPenyakit };
  });

  const animatedComponents = makeAnimated();

  return (
    <>
      <form onSubmit={handleSimpan}>
        <div className="row mb-3">
          <label htmlFor="idGejala" className="col-sm-2 col-form-label">
            Nama Penyakit
          </label>
          <div className="col-sm-6">
            <Select name="idPenyakit" isSearchable={true} isClearable={true} isDisabled={isDisabled} options={options2} components={animatedComponents} onChange={handleChangePenyakit}/>
          </div>
        </div>
        <div className="row mb-3">
          <label htmlFor="namaGejala" className="col-sm-2 col-form-label">
            Nama Gejala
          </label>
          <div className="col-sm-6">
            <Select name="idGejala" options={options} isMulti closeMenuOnSelect={false} components={animatedComponents} onChange={handleChangeGejala}/>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-2"></div>
          <div className="col-sm-6 text-end">
            <button type="submit" className="btn btn-primary mx-1">
              Simpan
            </button>
            <button
              type="button"
              id="cancel"
              onClick={handleCancel}
              className="btn btn-secondary mx-1"
              style={{ display: "none" }}
            >
              Batal
            </button>
          </div>
        </div>
      </form>

      <div className="row mt-3">
        <div className="col-8">
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Nama Penyakit</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {dataAturan.map((aturan) => {
                return (
                  <tr key={aturan.idPenyakit}>
                    <td>{aturan.idPenyakit}</td>
                    <td>{aturan.idPenyakit}</td>
                    <td className="text-end">
                      <button
                        className="btn btn-sm btn-info mx-1"
                        onClick={() => handleEdit(aturan.idPenyakit)}
                      >
                        edit
                      </button>
                      <button
                        className="btn btn-sm btn-danger"
                        onClick={() => handleDelete(aturan.idPenyakit)}
                      >
                        hapus
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
