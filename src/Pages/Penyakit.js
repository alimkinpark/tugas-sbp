import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addPenyakit, deletePenyakit, getPenyakit } from '../features/penyakit/penyakitSlice'

export default function Penyakit() {
  const dataPenyakit = useSelector(getPenyakit);
  const dispatch = useDispatch();

  let inputId;
  let btnCancel;

  const [penyakit, setPenyakit] = useState({
    idPenyakit: "",
    namaPenyakit: "",
  });

  const handleChange = (event) => {
    setPenyakit({ ...penyakit, [event.target.name]: event.target.value });
  };

  const handleSimpan = (event) => {
    event.preventDefault();

    dispatch(addPenyakit(penyakit))
    handleCancel()
  };

  const handleEdit = (id) => {
    inputId = document.querySelector("input[name=idPenyakit]");
    btnCancel = document.querySelector("#cancel");
    inputId.disabled = true;
    btnCancel.style.display = "inline";

    const data = dataPenyakit
      .filter((ms) => {
        return ms.idPenyakit === id;
      })
      .map((newPy) => {
        return newPy;
      });

    // console.log(data);
    setPenyakit(data[0]);
  };

  const handleCancel = () => {
    inputId = document.querySelector("input[name=idPenyakit]");
    btnCancel = document.querySelector("#cancel");
    inputId.disabled = false;
    btnCancel.style.display = "none";
    setPenyakit({ idPenyakit: "", namaPenyakit: "" });
  };

  const handleDelete = (id) => {
      dispatch(deletePenyakit(id));
  }

  return (
    <>
      <form onSubmit={handleSimpan}>
        <div className="row mb-3">
          <label htmlFor="idPenyakit" className="col-sm-2 col-form-label">
            ID Penyakit
          </label>
          <div className="col-sm-6">
            <input
              value={penyakit.idPenyakit}
              required
              type="text"
              className="form-control"
              id="idPenyakit"
              name="idPenyakit"
              onChange={(e) => handleChange(e)}
            />
          </div>
        </div>
        <div className="row mb-3">
          <label htmlFor="namaPenyakit" className="col-sm-2 col-form-label">
            Nama Penyakit
          </label>
          <div className="col-sm-6">
            <input
              value={penyakit.namaPenyakit}
              required
              type="text"
              className="form-control"
              id="namaPenyakit"
              name="namaPenyakit"
              onChange={(e) => handleChange(e)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-2"></div>
          <div className="col-sm-6 text-end">
            <button type="submit" className="btn btn-primary mx-1">
              Simpan
            </button>
            <button type="button" id="cancel" onClick={handleCancel} className="btn btn-secondary mx-1" style={{display:'none'}}>
              Batal
            </button>
          </div>
        </div>
      </form>

      <div className="row mt-3">
        <div className="col-8">
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Nama Penyakit</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {dataPenyakit.map((penyakit) => {
                return (
                  <tr key={penyakit.idPenyakit}>
                    <td>{penyakit.idPenyakit}</td>
                    <td>{penyakit.namaPenyakit}</td>
                    <td className="text-end">
                      <button className="btn btn-sm btn-info mx-1" onClick={() => handleEdit(penyakit.idPenyakit)}>edit</button>
                      <button className="btn btn-sm btn-danger" onClick={() => handleDelete(penyakit.idPenyakit)}>
                        hapus
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
