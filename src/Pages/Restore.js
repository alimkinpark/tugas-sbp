export default function Restore() {
  const handleClick = () => {
    const dataGejala = require('../features/gejala/ListGejala.json');
    const dataPenyakit = require('../features/penyakit/ListPenyakit.json');
    const text = "Restore Database ? \nData yang tersimpan sebelumnya akan hilang ! \nKlik Ok untuk Melanjutkan."

    if(window.confirm(text)==true) {
        localStorage.setItem('gejala', JSON.stringify(dataGejala));
        localStorage.setItem('penyakit', JSON.stringify(dataPenyakit));
    }

  };
  return (
    <div>
      <p>Restore Database</p>
      <button onClick={handleClick} className="btn btn-primary">Proses</button>
    </div>
  );
}
