import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addGejala, deleteGejala, getGejala } from '../features/gejala/gejalaSlice'

export default function Gejala() {
  const dataGejala = useSelector(getGejala);
  const dispatch = useDispatch();

  let inputId;
  let btnCancel;

  const [gejala, setGejala] = useState({
    idGejala: "",
    namaGejala: "",
  });

  const handleChange = (event) => {
    setGejala({ ...gejala, [event.target.name]: event.target.value });
  };

  const handleSimpan = (event) => {
    event.preventDefault();

    dispatch(addGejala(gejala))
    handleCancel()
  };

  const handleEdit = (id) => {
    inputId = document.querySelector("input[name=idGejala]");
    btnCancel = document.querySelector("#cancel");
    inputId.disabled = true;
    btnCancel.style.display = "inline";

    const data = dataGejala
      .filter((ms) => {
        return ms.idGejala === id;
      })
      .map((newPy) => {
        return newPy;
      });

    // console.log(data);
    setGejala(data[0]);
  };

  const handleCancel = () => {
    inputId = document.querySelector("input[name=idGejala]");
    btnCancel = document.querySelector("#cancel");
    inputId.disabled = false;
    btnCancel.style.display = "none";
    setGejala({ idGejala: "", namaGejala: "" });
  };

  const handleDelete = (id) => {
      dispatch(deleteGejala(id));
  }

  return (
    <>
      <form onSubmit={handleSimpan}>
        <div className="row mb-3">
          <label htmlFor="idGejala" className="col-sm-2 col-form-label">
            ID Gejala
          </label>
          <div className="col-sm-6">
            <input
              value={gejala.idGejala}
              required
              type="text"
              className="form-control"
              id="idGejala"
              name="idGejala"
              onChange={(e) => handleChange(e)}
            />
          </div>
        </div>
        <div className="row mb-3">
          <label htmlFor="namaGejala" className="col-sm-2 col-form-label">
            Nama Gejala
          </label>
          <div className="col-sm-6">
            <input
              value={gejala.namaGejala}
              required
              type="text"
              className="form-control"
              id="namaGejala"
              name="namaGejala"
              onChange={(e) => handleChange(e)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-2"></div>
          <div className="col-sm-6 text-end">
            <button type="submit" className="btn btn-primary mx-1">
              Simpan
            </button>
            <button type="button" id="cancel" onClick={handleCancel} className="btn btn-secondary mx-1" style={{display:'none'}}>
              Batal
            </button>
          </div>
        </div>
      </form>

      <div className="row mt-3">
        <div className="col-8">
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Nama Gejala</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {dataGejala.map((gejala) => {
                return (
                  <tr key={gejala.idGejala}>
                    <td>{gejala.idGejala}</td>
                    <td>{gejala.namaGejala}</td>
                    <td className="text-end">
                      <button className="btn btn-sm btn-info mx-1" onClick={() => handleEdit(gejala.idGejala)}>edit</button>
                      <button className="btn btn-sm btn-danger" onClick={() => handleDelete(gejala.idGejala)}>
                        hapus
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}