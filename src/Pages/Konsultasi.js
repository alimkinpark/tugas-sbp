import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getGejala } from "../features/gejala/gejalaSlice";
import { getPenyakit } from "../features/penyakit/penyakitSlice";
import {
  addAturan,
  deleteAturan,
  getAturan,
} from "../features/aturan/aturanSlice";

import Select from "react-select";
import makeAnimated from "react-select/animated";

export default function Konsultasi() {
  const dataGejala = useSelector(getGejala);
  const dataPenyakit = useSelector(getPenyakit);
  const dataAturan = useSelector(getAturan);
  const dispatch = useDispatch();

  let btnCancel;
  const [aturan, setAturan] = useState({
    idPenyakit: "",
    idGejala: [],
  });

  const [isDisabled, setIsDisabled] = useState(false);
  const [listGejala, setListGejala] = useState([]);
  const [namaGejala, setNamaGejala] = useState([]);

  const [isShow, setIsShow] = useState('d-none');
  const [showButton, setShowButton] = useState('d-none');

  const handleChangePenyakit = (event) => {
    // console.log(event)
    setAturan({ ...aturan, idPenyakit: event.value });
  };

  //   const handleChangeGejala = (event) => {
  //     // console.log(event)
  //     // setGejala(Array.isArray(event) ? event.map(x=>x.value):[])
  //     setAturan({
  //       ...aturan,
  //       idGejala: Array.isArray(event) ? event.map((x) => x.value) : [],
  //     });

  //     if (event.length) {
  //       setIsDisabled(true);
  //     } else {
  //       setIsDisabled(false);
  //     }
  //   };

  //   const handleSimpan = (event) => {
  //     event.preventDefault();

  //     dispatch(addAturan(aturan));
  //     handleCancel();
  //   };

  //   const handleEdit = (id) => {
  //     btnCancel = document.querySelector("#cancel");
  //     btnCancel.style.display = "inline";

  //     const data = dataAturan
  //       .filter((ms) => {
  //         return ms.idPenyakit === id;
  //       })
  //       .map((newPy) => {
  //         return newPy;
  //       });

  //     // console.log(data);
  //     setIsDisabled(true);
  //     setAturan(data[0]);
  //   };

  //   const handleCancel = () => {
  //     btnCancel = document.querySelector("#cancel");
  //     btnCancel.style.display = "none";
  //     setIsDisabled(false);
  //     setAturan({ idPenyakit: "", idGejala: [] });
  //   };

  //   const handleDelete = (id) => {
  //     dispatch(deleteAturan(id));
  //   };

  const options = dataGejala.map((gejala) => {
    return { value: gejala.idGejala, label: gejala.namaGejala };
  });

  const options2 = dataPenyakit.map((penyakit) => {
    return { value: penyakit.idPenyakit, label: penyakit.namaPenyakit };
  });

  const animatedComponents = makeAnimated();

  const handleGetAturan = () => {
    const x = dataAturan.filter((at) => {
      return at.idPenyakit === aturan.idPenyakit;
    });

    setAturan({
      ...aturan,
      idGejala: x,
    });

    if (x.length) {
        setListGejala(x[0].idGejala);
        setShowButton('d-block')
    } else {
        alert('data aturan tidak ada !');
    }

    // setNamaGejala()
  };

  const getGejalaName = (id) => {
    const x = dataGejala.filter((at) => {
      return at.idGejala === id;
    });

    // console.log(x)

    return x[0].namaGejala;
  };

  const submitKonsultan = () => {
    setIsShow('d-block')
  };

  return (
    <>
      <form>
        <div className="row mb-3">
          <label htmlFor="idKonsultasi" className="col-sm-2 col-form-label">
            Kode Konsultasi
          </label>
          <div className="col-sm-6">
            <input
              type={"text"}
              name="idKonsultasi"
              id="idKonsultasi"
              className="form-control"
            />
          </div>
        </div>
        <div className="row mb-3">
          <label htmlFor="Nama Pasien" className="col-sm-2 col-form-label">
            Nama Pasien
          </label>
          <div className="col-sm-6">
            <input
              type={"text"}
              name="namaPasien"
              id="namaPasien"
              className="form-control"
            />
          </div>
        </div>
        <div className="row mb-3">
          <label htmlFor="idGejala" className="col-sm-2 col-form-label">
            Nama Penyakit
          </label>
          <div className="col-sm-6">
            <Select
              name="idPenyakit"
              isSearchable={true}
              isClearable={true}
              isDisabled={isDisabled}
              options={options2}
              components={animatedComponents}
              onChange={handleChangePenyakit}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-2"></div>
          <div className="col-sm-6 text-end">
            <button
              type="button"
              onClick={handleGetAturan}
              className="btn btn-primary mx-1"
            >
              Next
            </button>
          </div>
        </div>

        <div className="row mt-4">
          <div className="col-12 mb-3">
            <h3>Gejala yang dialami</h3>
          </div>

          {listGejala &&
            listGejala.map((dat) => {
              return (
                <div className="row mb-2" key={dat}>
                  <div className="col-4">
                    {/* <label>{dat}</label> */}
                    <label>{getGejalaName(dat)}</label>
                  </div>

                  <div className="col-6">
                    <input
                      type="radio"
                      className="btn-check"
                      name={dat}
                      id={`yes-${dat}`}
                      autoComplete="off"
                      defaultChecked
                    />
                    <label
                      className="btn btn-outline-success mx-1"
                      htmlFor={`yes-${dat}`}
                    >
                      Ya
                    </label>

                    <input
                      type="radio"
                      className="btn-check"
                      name={dat}
                      id={`no-${dat}`}
                      autoComplete="off"
                    />
                    <label
                      className="btn btn-outline-danger"
                      htmlFor={`no-${dat}`}
                    >
                      Tidak
                    </label>
                  </div>
                </div>
              );
            })}
        </div>

        <div className={`row mt-4 ${showButton}`}>
          <div className="col-sm-2"></div>
          <div className="col-sm-6 text-end">
            <button
              type="button"
              onClick={submitKonsultan}
              className="btn btn-primary mx-1"
            >
              Submit
            </button>
            <button
              type="button"
              //   onClick={handleGetAturan}
              className="btn btn-secondary mx-1"
            >
              Cancel
            </button>
          </div>
        </div>
      </form>

      <div className={`alert alert-success mt-4 ${isShow}`} role="alert">
        <h4 className="alert-heading">Hasil!</h4>
        <p>
          Iya, anda Mengalami sakit Tersebut. !
        </p>
      </div>
    </>
  );
}
